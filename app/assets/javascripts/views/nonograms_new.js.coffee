$ ->
  # find elements
  widthInput = $('select#nonogram_width')
  heightInput = $('select#nonogram_height')
  saveButton = $('button#save')

  # set board & cell sizes
  $.n.boardSize.width = widthInput.val()
  $.n.boardSize.height = heightInput.val()

  updateCellArray = () ->
    $.n.cellStatusArray = $.arrayBuilder($.n.boardSize.width, $.n.boardSize.height)

  # draw initial board
  updateCellArray()
  $.n.setCanvasSize()
  $.n.drawCellGrid()
  $.n.drawSectionGrid()

  # events
  widthInput.change () ->
    $.n.boardSize.width = $(@).val()
    updateCellArray()
    $.n.setCanvasSize()
    $.n.drawCellGrid()
    $.n.drawSectionGrid()

  heightInput.change () ->
    $.n.boardSize.height = $(@).val()
    updateCellArray()
    $.n.setCanvasSize()
    $.n.drawCellGrid()
    $.n.drawSectionGrid()

  saveButton.click () ->
    form = $('form#new_nonogram').serialize() + '&' + $.param({data: $.n.cellStatusArray})

    $.post nonogramsPath,
      form
