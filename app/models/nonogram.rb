class NonogramDataValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    # validates the height dimension of the array
    unless value.map{ |e| e.length }.uniq.length == 1
      record.errors[attribute] << 'is malformed. The nested arrays have varying amounts of elements!'
    end

    # validates width & height ranges
    unless record.from_image
      unless (5..50).step(5).include?(record.width) && (5..50).step(5).include?(record.height)
        record.errors[attribute] << 'width & height must be a multiple of 5, between 5 & 50'
      end
    end
  end
end

class Nonogram
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name
  field :data, :type => Array, :default => [[]]
  field :from_image, :type => Boolean, :default => false

  attr_accessible :name, :data, :from_image

  validates :name, :presence => true
  validates :data, :nonogram_data => true

  def width
    data[0].length
  end

  def height
    data.length
  end

  # calculates the clues based on the puzzle data
  def clues
    {
      :row => calculate_clues(data),
      :col => calculate_clues(data.transpose)
    }
  end

  private

  def calculate_clues array
    clue_array = []
    array.each do |e|
      a = []
      count = 0
      current_cell = nil
      e.each do |c|
        current_cell = c
        if c == 0
          a << count if count > 0
          count = 0
        else
          count += 1
        end
      end
      a << count if count > 0 && current_cell == 1
      clue_array << a
    end
    clue_array
  end

  # class methods
  def self.convert_js_data js_data
    new_array = []
    js_data.each do |col, row_array|
      new_array << row_array.map{ |e| e.to_i }
    end
    new_array
  end
end
