require "spec_helper"

describe NonogramsController do
  describe "routing" do

    it "routes to #index" do
      get("/nonograms").should route_to("nonograms#index")
    end

    it "routes to #new" do
      get("/nonograms/new").should route_to("nonograms#new")
    end

    it "routes to #show" do
      get("/nonograms/1").should route_to("nonograms#show", :id => "1")
    end

    it "routes to #edit" do
      get("/nonograms/1/edit").should route_to("nonograms#edit", :id => "1")
    end

    it "routes to #create" do
      post("/nonograms").should route_to("nonograms#create")
    end

    it "routes to #update" do
      put("/nonograms/1").should route_to("nonograms#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/nonograms/1").should route_to("nonograms#destroy", :id => "1")
    end

  end
end
