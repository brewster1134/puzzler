require 'spec_helper'

describe Nonogram do
  it { should have_field(:name) }
  it { should have_field(:data).of_type(Array).with_default_value_of([[]]) }
  # mongoid-rspec 1.4.6 (which depends on mongoid 3.x)
  # it { should custom_validate(:data).with_validator(NonogramDataValidator) }

  context 'when data is valid' do
    let(:nonogram) { FactoryGirl.create(:nonogram) }

    it 'should return width' do
      nonogram.width.should == 10
    end

    it 'should return height' do
      nonogram.height.should == 5
    end
  end

  context 'when data is malformed' do
    malformed_array = FactoryGirl.attributes_for(:nonogram)[:data].deep_dup
    malformed_array.last.pop

    context 'when not from an image' do
      let(:nonogram) { FactoryGirl.build(:nonogram, :data => malformed_array) }

      it 'should be invalid' do
        nonogram.should_not be_valid
        nonogram.should have(1).error_on(:data)
      end
    end

    context 'when from an image' do
      let(:nonogram) { FactoryGirl.build(:nonogram, :data => malformed_array, :from_image => true) }

      it 'should be invalid even if from an image' do
        nonogram.should_not be_valid
        nonogram.should have(1).error_on(:data)
      end
    end
  end

  context 'when data is invalid' do
    invalid_array = FactoryGirl.attributes_for(:nonogram)[:data].deep_dup
    invalid_array.each{ |a| a.pop }
    let(:nonogram) { FactoryGirl.build(:nonogram, :data => invalid_array) }

    it 'should be invalid' do
      nonogram.should_not be_valid
      nonogram.should have(1).error_on(:data)
    end
  end

  context 'when solving the puzzle' do
    let(:nonogram) { FactoryGirl.create(:nonogram, :data => [[1, 0, 0, 1, 0], [1, 0, 0, 0, 0], [1, 1, 1, 0, 1], [0, 1, 1, 1, 1], [0, 0, 0, 0, 0]]) }
    it 'should generate a clues array' do
      nonogram.clues.should == {
        :row => [[1,1], [1], [3,1], [4], []],
        :col => [[3], [2], [2], [1,1], [2]]
      }
    end
  end

  context 'class methods' do
    subject { Nonogram }
    let(:data) do
      {"0"=>["0", "1", "0"], "1"=>["1", "0", "1"], "2"=>["0", "1", "0"]}
    end

    it 'should convert data from the JS' do
      subject.convert_js_data(data).should == [
        [0,1,0],
        [1,0,1],
        [0,1,0],
      ]
    end
  end

  context 'when using an image' do
    let(:nonogram) { FactoryGirl.create(:nonogram, :from_image => true, :data => [[1, 0, 0], [1, 0, 0]]) }

    it 'should not validate height & width being multiples of 5' do
      nonogram.should be_valid
      nonogram.should have(0).error_on(:data)
    end
  end
end
