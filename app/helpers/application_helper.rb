module ApplicationHelper
  ACTION_MAP = {
    :update => :edit,
    :create => :new
  }.wia

  # renders a custom class for the body element for better scoping CSS
  def body_class
    [controller_name, ACTION_MAP[action_name] || action_name] * " "
  end

  def ascii string, *args
    options = args.extract_options!
    options.reverse_merge!({
        :font => Artii::Base.new().all_fonts.keys.sample
      })
    artii = Artii::Base.new(options) rescue Artii::Base.new() # for Artii bug
    ascii = artii.asciify(string)

    simple_format ascii, :class => 'ascii'
  end

  # includes view specific CSS & JS files
  #
  # for example, if the update action for the places controller is rendered,
  # it will look for the following files within the asset pipeline in a directory
  # called `/views`
  #
  # * places.[js|css]
  # * places_edit.[js|css]
  #
  # notice that the action it looks up for update is actually edit.  this mapping
  # is defined in the ACTION_MAP constant
  def view_asset type
    raise ArgumentError, 'type must be :css or :js' unless [:css, :js].include? type
    controller  = "views/#{controller_name}.#{type}"
    action      = "views/#{controller_name}_#{ACTION_MAP[action_name] || action_name}.#{type}"
    assets      = [(controller if Rails.application.assets[controller]), (action if Rails.application.assets[action])].compact
    return nil if assets.empty?

    case type
    when :css then stylesheet_link_tag *assets
    when :js  then javascript_include_tag *assets
    end
  end
end

# render flash messages
def render_flash
  html = ''
  flash.each{ |type, msg| html << content_tag(:div, msg, :class => type) }
  html.html_safe
end
