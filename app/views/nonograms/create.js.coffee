<%- if @nonogram.save %>
  <%- flash[:success] = 'Nonogram saved!' %>
  window.location.replace "<%= url_for @nonogram %>"
<%- else %>
  $('body .flash').prepend("<%= j render 'layouts/validation_errors', :object => @nonogram %>")
<%- end %>
