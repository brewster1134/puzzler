Puzzler::Application.routes.draw do
  resources :nonograms
  match 'nonograms/image' => 'nonograms#image', :via => :post

  root :to => 'home#index', :via => :get
end
