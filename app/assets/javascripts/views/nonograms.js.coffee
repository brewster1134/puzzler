# settings
$.nonogram =
  cellMaxPixels: 30 # the max size a cell can be
  cellBorderWidth: 1
  cellSelectedColor: 'grey'
  sectionSize: 5 # how many cells in between the bold section lines
  sectionBorderWidth: 3
  boardSize:
    width: 15
    height: 10

  # app settings. DO NOT CHANGE!
  draw: false # set drawing to be false by default
  selectedCell: null
  setCellTo: 1
  cellStatusArray: [[]]
  drawMap: (status) ->
    switch status
      when 0 then 1
      when 1 then 0

$.n = $.nonogram
$.n.cellOffset = Math.floor($.n.sectionBorderWidth / 2) + 0.5 # offsets drawing cells in the canvas to allow outer bold section lines & sharper cell lines
$.n.maxCellSize = $.n.cellMaxPixels
$.n.cellSize = $.n.maxCellSize

# builds a new cell array and resizes the canvas to match based on the
# specified board size.
$.n.setCanvasSize = () ->
  $.n.maxCellSize = Math.floor($.n.canvas.parent().width() / $.n.boardSize.width)
  $.n.cellSize = if $.n.maxCellSize > $.n.cellMaxPixels then $.n.cellMaxPixels else $.n.maxCellSize
  canvasSize =
    width: ($.n.cellSize * $.n.boardSize.width) + $.n.sectionBorderWidth
    height: ($.n.cellSize * $.n.boardSize.height) + $.n.sectionBorderWidth

  $.n.canvas.attr 'width', canvasSize.width
  $.n.canvas.attr 'height', canvasSize.height

# draw grid for individual cells
$.n.drawCellGrid = () ->
  cellRowPosition = $.n.cellOffset
  cellColPosition = $.n.cellOffset
  ctx = $.n.canvas[0].getContext('2d')
  ctx.lineWidth = $.n.cellBorderWidth

  # draw rows
  for row in [1..($.n.boardSize.height + 1)]
    ctx.beginPath()
    ctx.moveTo 0, cellRowPosition
    ctx.lineTo $.n.canvas.width(), cellRowPosition
    cellRowPosition += $.n.cellSize
    ctx.stroke()

  for col in [1..($.n.boardSize.width + 1)]
    ctx.beginPath()
    ctx.moveTo cellColPosition, 0
    ctx.lineTo cellColPosition, $.n.canvas.height()
    cellColPosition += $.n.cellSize
    ctx.stroke()

# draw sections lines
$.n.drawSectionGrid = () ->
  cellRowPosition = $.n.cellOffset
  cellColPosition = $.n.cellOffset
  ctx = $.n.canvas[0].getContext('2d')
  ctx.strokeStyle = 'black'
  ctx.lineWidth = $.n.sectionBorderWidth

  # draw rows
  for row in [1..(($.n.boardSize.height / $.n.sectionSize) + 1)]
    ctx.beginPath()
    ctx.moveTo 0, cellRowPosition
    ctx.lineTo $.n.canvas.width(), cellRowPosition
    cellRowPosition += $.n.cellSize * $.n.sectionSize
    ctx.stroke()

  for col in [1..(($.n.boardSize.width / $.n.sectionSize) + 1)]
    ctx.beginPath()
    ctx.moveTo cellColPosition, 0
    ctx.lineTo cellColPosition, $.n.canvas.height()
    cellColPosition += $.n.cellSize * $.n.sectionSize
    ctx.stroke()

# draw cells from array
$.n.drawCellsFromArray = () ->
  $.each $.n.cellStatusArray, (rowIndex) ->
    $.each $(@), (colIndex) ->
      cellStatus = $.n.cellStatusArray[rowIndex][colIndex]
      $.n.drawCell rowIndex, colIndex, cellStatus

# return coordinates of where mouse was clicked within the game board
$.n.getMousePos = (e) ->
  obj = $.n.canvas[0]
  top = 0
  left = 0

  while obj && obj.tagName != 'BODY'
    top += obj.offsetTop
    left += obj.offsetLeft
    obj = obj.offsetParent

  # return relative mouse position
  mouseX = e.clientX - left + window.pageXOffset
  mouseY = e.clientY - top + window.pageYOffset
  {
    x: mouseX,
    y: mouseY
  }

# find the cell index based on the mouse coordinates
$.n.findCell = (mousePos) ->
  {
    row: Math.floor(mousePos.y / $.n.cellSize)
    col: Math.floor(mousePos.x / $.n.cellSize)
  }

$.n.getCellStatus = (row, col) ->
  $.n.cellStatusArray[row][col]

# update array with new cell status
$.n.drawCell = (row, col, status) ->
  $.n.cellStatusArray[row][col] = status
  xPos = ($.n.cellSize * (col)) + $.n.cellOffset
  yPos = ($.n.cellSize * (row)) + $.n.cellOffset

  switch status
    when 0 # draw blank space
      $.n.ctx.lineWidth = $.n.cellBorderWidth
      $.n.ctx.fillStyle = 'white'
      $.n.ctx.fillRect xPos, yPos, $.n.cellSize, $.n.cellSize
      $.n.ctx.strokeRect xPos, yPos, $.n.cellSize, $.n.cellSize
    when 1 # draw filled space
      $.n.ctx.lineWidth = $.n.cellBorderWidth
      $.n.ctx.fillStyle = $.n.cellSelectedColor
      $.n.ctx.fillRect xPos, yPos, $.n.cellSize, $.n.cellSize
      $.n.ctx.strokeRect xPos, yPos, $.n.cellSize, $.n.cellSize
    when 2 # draw dot
      $.n.ctx.lineWidth = $.n.cellBorderWidth
      $.n.ctx.fillStyle = 'white'
      $.n.ctx.fillRect xPos, yPos, $.n.cellSize, $.n.cellSize
      $.n.ctx.strokeRect xPos, yPos, $.n.cellSize, $.n.cellSize
      $.n.ctx.beginPath()
      $.n.ctx.fillStyle = 'black'
      $.n.ctx.arc xPos + ($.n.cellSize / 2), yPos + ($.n.cellSize / 2), $.n.cellSize / 10, 0, Math.PI*2, true
      $.n.ctx.closePath()
      $.n.ctx.fill()
    when 3 # error
      lineWidth = $.n.cellSize / 5
      $.n.ctx.lineWidth = lineWidth
      $.n.ctx.strokeStyle = 'red'
      $.n.ctx.strokeRect xPos + (lineWidth / 2), yPos + (lineWidth / 2), $.n.cellSize - lineWidth, $.n.cellSize - lineWidth

$ ->
  # find elements
  $.n.canvas = $('canvas#nonogram')

  if $.n.canvas[0] && $.n.ctx = $.n.canvas[0].getContext('2d')
    # drawing events
    $.n.canvas.mousedown (e) ->
      e.originalEvent.preventDefault() # chrome fix to prevent changing to a text cursor when dragging
      $.n.draw = true
    $.n.canvas.mouseup () ->
      $.n.draw = false
    $.n.canvas.bind 'mousemove mousedown', (e) ->
      if $.n.draw
        selectedCell = $.n.findCell $.n.getMousePos(e)
        if e.type == 'mousedown'
          $.n.setCellTo = $.n.drawMap($.n.getCellStatus(selectedCell.row, selectedCell.col))
        $.n.drawCell selectedCell.row, selectedCell.col, $.n.setCellTo
        $.n.drawSectionGrid()
