require 'spec_helper'

describe "nonograms/new" do
  before do
    assign(:nonogram, FactoryGirl.build(:nonogram))
    render
  end

  it "renders a canvas" do
    assert_select "canvas"
  end

  it "renders the form elements" do
    assert_select 'select', :id => 'nonogram_width'
    assert_select 'select', :id => 'nonogram_height'
    assert_select 'input', :type => :file, :id => 'nonogram_image'
    assert_select 'input', :type => 'text', :id => 'nonogram_name'
    assert_select 'button', :type => 'submit', :id => 'save'
  end
end
