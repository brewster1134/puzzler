$ ->
  # find elements
  saveButton = $('button#save')
  $.n.cellStatusArray = $.parseJSON cellStatus
  $.n.boardSize.width = $.n.cellStatusArray[0].length
  $.n.boardSize.height = $.n.cellStatusArray.length

  $.n.setCanvasSize()
  $.n.drawCellsFromArray()
  $.n.drawSectionGrid()

  saveButton.click () ->
    form = $('form[id^=edit_nonogram]').serialize() + '&' + $.param({data: $.n.cellStatusArray})

    $.ajax
      url: nonogramPath,
      type: 'PUT',
      data:
        form
