#= require jquery
#= require jquery_ujs
#= require jquery.cookie
#= require bootstrap/js/bootstrap.min
#= require_self

$ = jQuery

# builds a 2 dimensional array padded with elements for width & height
# the width becomes the 1st dimension of the arry, and the height the 2nd
# for example:
# arrayBuilder(2, 3)
# => [[0, 0, 0], [0, 0, 0]]
$.arrayBuilder = (width, height, val = 0) ->
  x = new Array(parseInt(height))
  $(x).each (i) ->
    y = new Array(parseInt(width))
    $(y).each (i) ->
      y[i] = val
    x[i] = y
  x

# render flash messages via JS
$.flash = (msg, type = 'alert') ->
  $('body .flash div').remove()
  $(document.createElement('div')).addClass(type).text(msg).prependTo($('body .flash'))
