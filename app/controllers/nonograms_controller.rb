class NonogramsController < ApplicationController
  def index
    @nonograms = Nonogram.all
  end

  def show
    @nonogram = Nonogram.find params[:id]
    @clues = @nonogram.clues
  end

  def new
    @nonogram = Nonogram.new
  end

  def create
    @nonogram = Nonogram.new params[:nonogram]
    @nonogram.data = Nonogram.convert_js_data params[:data]
  end

  def edit
    @nonogram = Nonogram.find params[:id]
  end

  def update
    @nonogram = Nonogram.find params[:id]
    @nonogram.update_attributes params[:nonogram]
    @nonogram.data = Nonogram.convert_js_data params[:data]
  end

  def destroy
    @nonogram = Nonogram.find params[:id]
    @nonogram.destroy

    redirect_to nonograms_path
  end

  def image
    tmp = params[:nonogram][:image]
    img = Magick::Image.read(tmp.tempfile.path).first
    new_image = img.resize_to_fit(50, 50).threshold(Magick::QuantumRange*0.9)

    pixel_array = []
    new_image.rows.times { |x| pixel_array << Array.new(new_image.columns, 0) }

    new_image.each_pixel do |pixel, col, row|
      cell_value = case pixel.to_color
      when 'white' then 0
      when 'black' then 1
      end
      pixel_array[row][col] = cell_value
    end
    @nonogram = Nonogram.new :name => tmp.original_filename, :data => pixel_array, :from_image => true

    if @nonogram.save
      flash[:success] = 'Image converted!'
      redirect_to edit_nonogram_path(@nonogram)
    else
      flash[:error] = 'Image could not be converted.'
      render :new
    end
  end
end
