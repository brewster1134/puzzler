$ ->
  solvedArray = $.parseJSON cell_status
  $.n.boardSize.width = solvedArray[0].length
  $.n.boardSize.height = solvedArray.length
  $.n.cellStatusArray = if $.cookie(nonogram_id)
    $.parseJSON $.cookie(nonogram_id)
  else
    $.arrayBuilder $.n.boardSize.width, $.n.boardSize.height
  $.n.drawMap = (status) ->
    switch status
      when 0 then 1
      when 1 then 2
      when 2 then 0
      when 3 then 0


  $.n.setCanvasSize()
  if $.cookie(nonogram_id)
    $.n.drawCellsFromArray()
  else
    $.n.drawCellGrid()
  $.n.drawSectionGrid()

  $('td.col_clues td').css('width', $.n.cellSize)
  $('td.row_clues td').css('height', $.n.cellSize)
  $('table.master').css('width', 'inherit')

  $('button#save').click () ->
    if $.cookie nonogram_id, JSON.stringify($.n.cellStatusArray)
      $.flash 'Progress saved in a cookie, so dont clear them! and use your same browser to resume!', 'success'

  $('button#error_check').click () ->
    error_count = 0
    perfect = false
    $.each $.n.cellStatusArray, (rowIndex) ->
      $.each $(@), (colIndex, status) ->
        correct = solvedArray[rowIndex][colIndex]
        if status != correct
          perfect = false
        if (status == 1 && correct != 1) || (status == 2 && correct != 0) || status == 3
          error_count++
          $.n.drawCell rowIndex, colIndex, 3
    $.n.drawSectionGrid()
    if perfect
      alert 'SUCCESS! You have finished ' + nongoram_name
      $('li.name').text(nongoram_name)
    else if error_count == 0
      $.flash 'No Errors!', 'success'
    else if error_count > 0
      $.flash 'Bummer! ' + error_count + ' error(s) found!', 'error'
    $('.flash div').delay(5000).fadeOut()

  $('button#reset').click () ->
    $.n.cellStatusArray = $.arrayBuilder $.n.boardSize.width, $.n.boardSize.height
    console.log = $.n.cellStatusArray
    $.n.drawCellsFromArray()
    $.n.drawSectionGrid()
