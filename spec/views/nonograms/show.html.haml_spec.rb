require 'spec_helper'

describe "nonograms/show" do
  let(:nonogram) { FactoryGirl.create(:nonogram) }
  before do
    assign(:nonogram, nonogram)
    assign(:clues, nonogram.clues)
    render
  end

  it "renders a canvas" do
    assert_select "canvas"
  end
end
