require 'rubygems'
require 'spork'

Spork.prefork do
  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'rspec/autorun'

  RSpec.configure do |config|
    # If true, the base class of anonymous controllers will be inferred
    # automatically. This will be the default behavior in future versions of
    # rspec-rails.
    config.infer_base_class_for_anonymous_controllers = false
    config.filter_run_excluding :js => true

    config.before :suite do
      DatabaseCleaner[:mongoid].strategy = :truncation
    end

    config.before :each do
      DatabaseCleaner.start
    end

    config.after :each do
      DatabaseCleaner.clean
    end
  end

end

Spork.each_run do
  ActiveSupport::Dependencies.clear
  FactoryGirl.reload

  # Requires supporting ruby files with custom matchers and macros, etc,
  # in spec/support/ and its subdirectories.
  Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

  RSpec.configure do |config|
    config.include Rails.application.routes.url_helpers
    config.include Mongoid::Matchers, :type => :model
    # config.include ControllersHelpers, :type => :controller
    # config.include ModelsHelpers, :type => :model
    # config.include RequestsHelpers, :type => :request
  end
end
