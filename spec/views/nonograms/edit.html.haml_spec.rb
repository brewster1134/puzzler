require 'spec_helper'

describe "nonograms/edit" do
  before(:each) do
    assign(:nonogram, FactoryGirl.create(:nonogram))
    render
  end

  it "renders a canvas" do
    assert_select "canvas"
  end

  it "renders the form elements" do
    assert_select 'button', :type => 'submit', :id => 'save'
  end
end
