require 'spec_helper'

describe "nonograms/index" do
  before do
    @nonogram = FactoryGirl.create(:nonogram)
    assign(:nonograms, [
      @nonogram
    ])
    render
  end

  it 'renders the nonograms' do
    rendered.should contain @nonogram.name
  end
end
