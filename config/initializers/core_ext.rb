class Hash
  alias :wia :with_indifferent_access
end

class Array
  def deep_dup
    Marshal.load(Marshal.dump(self))
  end
end
