require 'spec_helper'

describe NonogramsController do
  let(:valid_js_array) do
    {
      "0" => ["0","0","0","0","0"],
      "1" => ["0","0","0","0","0"],
      "2" => ["0","0","0","0","0"],
      "3" => ["0","0","0","0","0"],
      "4" => ["0","0","0","0","0"]
    }
  end
  let(:nonogram) { FactoryGirl.create(:nonogram) }

  describe "GET index" do
    it "assigns all nonograms as @nonograms" do
      get :index, {}
      assigns(:nonograms).should eq([nonogram])
    end
  end

  describe "GET show" do
    it "assigns the requested nonogram as @nonogram" do
      get :show, {:id => nonogram.to_param}
      assigns(:nonogram).should eq(nonogram)
    end
  end

  describe "GET new" do
    it "assigns a new nonogram as @nonogram" do
      get :new, {}
      assigns(:nonogram).should be_a_new(Nonogram)
    end
  end

  describe "GET edit" do
    it "assigns the requested nonogram as @nonogram" do
      get :edit, {:id => nonogram.to_param}
      assigns(:nonogram).should eq(nonogram)
    end
  end

  describe "POST create" do
    render_views

    describe "with valid params" do
      it "creates a new Nonogram" do
        expect {
          xhr :post, :create, {:nonogram => FactoryGirl.attributes_for(:nonogram), :data => valid_js_array}
        }.to change(Nonogram, :count).by(1)
      end

      it "assigns a newly created nonogram as @nonogram" do
        xhr :post, :create, {:nonogram => FactoryGirl.attributes_for(:nonogram), :data => valid_js_array}
        assigns(:nonogram).should be_a(Nonogram)
        assigns(:nonogram).should be_persisted
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "assigns the requested nonogram as @nonogram" do
        xhr :put, :update, {:id => nonogram.to_param, :data => valid_js_array}
        assigns(:nonogram).should eq(nonogram)
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested nonogram" do
      nonogram
      expect {
        delete :destroy, {:id => nonogram.to_param}
      }.to change(Nonogram, :count).by(-1)
    end

    it "redirects to the nonograms list" do
      delete :destroy, {:id => nonogram.to_param}
      response.should redirect_to(nonograms_path)
    end
  end

  describe 'POST image' do
    it 'should process the image' do
    end
  end
end
